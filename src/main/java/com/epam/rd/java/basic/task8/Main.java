package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Test;
import com.epam.rd.java.basic.task8.util.Sorter;

public class Main {

	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}

		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		// get
		DOMController domController = new DOMController(xmlFileName);
		domController.parse(true);
		Test test = domController.getTest();

		// sort (case 1)
		Sorter.sortQuestionsByQuestionText(test);

		// save
		String outputXmlFile = "output.dom.xml";
		DOMController.saveToXML(test, outputXmlFile);
		System.out.println("Output ==> " + outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////

		// get
		SAXController saxController = new SAXController(xmlFileName);
		saxController.parse(true);
		test = saxController.getTest();

		// sort  (case 2)
		Sorter.sortQuestionsByAnswersNumber(test);

		// save
		outputXmlFile = "output.sax.xml";

		// other way:
		DOMController.saveToXML(test, outputXmlFile);
		System.out.println("Output ==> " + outputXmlFile);

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////

		// get
		STAXController staxController = new STAXController(xmlFileName);
		staxController.parse();
		test = staxController.getTest();

		// sort  (case 3)
		Sorter.sortAnswersByContent(test);

		// save
		outputXmlFile = "output.stax.xml";
		DOMController.saveToXML(test, outputXmlFile);
		System.out.println("Output ==> " + outputXmlFile);
	}

}
