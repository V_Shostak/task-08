package com.epam.rd.java.basic.task8.constants;

public enum XML {
    // elements names
    TEST("Test"), FLOWER("Question"), DESCRIPTION("QuestionText"), ANSWER("Answer"),

    // attribute name
    CORRECT("correct");

    private String value;

    XML(String value) {
        this.value = value;
    }

    public boolean equalsTo(String name) {
        return value.equals(name);
    }

    public String value() {
        return value;
    }
}
